import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDropdownMenuPortalComponent } from './user-dropdown-menu-portal.component';

describe('UserDropdownMenuPortalComponent', () => {
  let component: UserDropdownMenuPortalComponent;
  let fixture: ComponentFixture<UserDropdownMenuPortalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDropdownMenuPortalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDropdownMenuPortalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
