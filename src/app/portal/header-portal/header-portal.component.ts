import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppService } from 'src/app/utils/services/app.service';
import { common } from 'src/app/app.common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header-portal',
  templateUrl: './header-portal.component.html',
  styleUrls: ['./header-portal.component.scss']
})
export class HeaderPortalComponent implements OnInit {
  @Output() toggleMenuSidebar: EventEmitter<any> = new EventEmitter<any>();
  public searchForm: FormGroup;
  com: common;
  UserName:string="Đang lấy dữ liệu..."
  constructor(private appService: AppService, public router: Router) {}

  ngOnInit() {
  }
  logout() {
    this.appService.logoutPortal();
  }
}
