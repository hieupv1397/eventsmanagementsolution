import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AppService } from 'src/app/utils/services/app.service';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { AccService } from 'src/app/utils/services/acc.service';

@Component({
  selector: 'app-register-portal',
  templateUrl: './register-portal.component.html',
  styleUrls: ['./register-portal.component.scss']
})
export class RegisterPortalComponent implements OnInit {
  public loginForm: FormGroup;
  constructor(
    private renderer: Renderer2,
    private toastr: ToastrService,
    private appService: AppService,
    private http: HttpClient,
    private cookieService: CookieService,
    private Acc: AccService
  ) { }
req:any;
  ngOnInit(): void {
    this.loginForm = new FormGroup({
      FullName: new FormControl(null, Validators.required),
      Email: new FormControl(null, Validators.required),
      UserName: new FormControl(null, Validators.required),
      Password: new FormControl(null, Validators.required),
    });
  }
  RegisterCustomer() {
    if (this.loginForm.valid) {
      // let req = {
      //   UserName: this.loginForm.controls.UserName.value,
      //   Password: this.loginForm.controls.Password.value,
      //   UserCategory: 1,
      // };
      let req= this.loginForm.value
      console.log(req)
      this.Acc.RegisterCustomer(req).subscribe((z) => {
        if (z.Status == 1) {
          this.toastr.success(z.Message)
        } else {
          this.toastr.error(z.Message, 'Tác vụ thất bại');
          localStorage.removeItem('PortalInfo');
        }
      });
    } else {
      this.toastr.error('Vui lòng nhập đầy đủ thông tin', 'Tác vụ thất bại');
    }
  }
}
