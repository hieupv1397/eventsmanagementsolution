import { Component, OnInit, Renderer2, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PortalService } from 'src/app/utils/services/portal.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { common } from 'src/app/app.common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-checkin',
  templateUrl: './checkin.component.html',
  styleUrls: ['./checkin.component.scss'],
})
export class CheckinComponent implements OnInit, OnDestroy {
  @ViewChild('contentWrapper', { static: false }) contentWrapper;
  public registerForm: FormGroup;
  public com: common;
  token:any;
  EventsID:any;
  closeResult: string;
  Noi_dung: string;
  constructor(
    private renderer: Renderer2,
    private toastr: ToastrService,
    private service: PortalService,
    public route: ActivatedRoute,
    public router: Router,
    private modalService: NgbModal,
  ) {}
  
  ngOnInit() {
    
    this.com = new common(this.router);
    this.com.CheckLoginPortal();
    var a = this.com.getPortalInfo();
    this.token = a.Token;
    this.route.queryParams.subscribe((params) => {
      this.EventsID = params.IDSuKien;
      this.GetEvent(this.EventsID, this.token);
      this.registerForm = new FormGroup({
        IDSuKien: new FormControl(params.IDSuKien),
        HoTen: new FormControl(null, Validators.required),
        SoDienThoai: new FormControl(null, Validators.required),
        Email: new FormControl(null, [Validators.required, Validators.email]),
        DiaChi: new FormControl(null, Validators.required),
      });
    });
  }
  register() {
    if (this.registerForm.valid) {
      this.CheckIn(this.registerForm.value);
    } else {
      this.toastr.error(
        'Thông tin không chính xác',
        'Vui lòng kiểm tra lại thông tin của bạn'
      );
    }
  }
  CheckIn(req) {
    this.service.CheckIn(req).subscribe((z) => {
      if (z.Status == 1) {
        this.toastr.success(z.Message);
      } else {
        this.toastr.warning(z.Message);
      }
    });
  }
  GetEvent(ID, token) {
    this.service.GetEvent(ID, token).subscribe((z) => {
      console.log(z);
      this.Noi_dung=z.Data.NoiDung;
      // this.Insert.patchValue({
      //   TenSuKien: z.Data.TenSuKien,
      //   NoiDung: z.Data.NoiDung,
      //   DiaDiem: z.Data.DiaDiem,
      //   GhiChu: z.Data.GhiChu,
      // });
    });
  }
  ngOnDestroy() {
    this.renderer.removeClass(
      document.querySelector('app-root'),
      'register-page'
    );
  }
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'lg', scrollable: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
