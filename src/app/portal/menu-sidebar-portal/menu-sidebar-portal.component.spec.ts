import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuSidebarPortalComponent } from './menu-sidebar-portal.component';

describe('MenuSidebarPortalComponent', () => {
  let component: MenuSidebarPortalComponent;
  let fixture: ComponentFixture<MenuSidebarPortalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuSidebarPortalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuSidebarPortalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
