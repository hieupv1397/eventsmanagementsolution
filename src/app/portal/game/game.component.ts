import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxWheelComponent, TextOrientation, TextAlignment } from 'ngx-wheel';
import { ActivatedRoute, Router } from '@angular/router';
import { PortalService } from 'src/app/utils/services/portal.service';
import { ToastrService } from 'ngx-toastr';
import { common } from 'src/app/app.common';
import { FormControl, FormGroup, FormBuilder, Validators, MaxLengthValidator, } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit {
  @ViewChild(NgxWheelComponent, { static: false }) wheel;
  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger = new Subject();
  constructor(
    private toastr: ToastrService,
    public router: Router,
    private service: PortalService,
    public route: ActivatedRoute
  ) { }
  seed = [...Array(12).keys()];
  idToLandOn: any;
  items: any[];
  textOrientation: TextOrientation = TextOrientation.HORIZONTAL;
  textAlignment: TextAlignment = TextAlignment.OUTER;
  EventsID: any;
  TableData: any;
  WinnerData: any;
  token: string;
  IDCustomer: any;
  public com: common;
  GuestIndex: number;
  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: true,
      dom: 'Bfrtip',
      // Configure the buttons
      buttons: ['excel'],
      language: {
        processing: 'Đang xử lý...',
        lengthMenu: 'Xem _MENU_ mục',
        emptyTable: 'Không có dữ liệu!',
        info: 'Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục',
        infoEmpty: 'Đang xem 0 đến 0 trong tổng số 0 mục',
        infoFiltered: '(được lọc từ _MAX_ mục)',
        infoPostFix: '',
        search: 'Tìm kiếm nhanh:',
        url: '',
        searchPlaceholder: 'Nhập từ khóa cần tìm...',
        paginate: {
          first: 'Đầu',
          previous: 'Trước',
          next: 'Tiếp',
          last: 'Cuối',
        },
      },
      columns: [
        {
          title: 'Mã khách',
        },
        {
          title: 'Họ tên',
        },
        {
          title: 'Số điện thoại',
        },
        {
          title: 'Email',
        },
        {
          title: 'Địa chỉ',
        },
      ],
    };
    this.idToLandOn = this.seed[Math.floor(Math.random() * this.seed.length)];
    const colors = ['#FF0000', '#000000'];
    this.items = this.seed.map((value) => ({
      fillStyle: colors[value % 2],
      text: `Prize ${value}`,
      id: value,
      textFillStyle: 'white',
      textFontSize: '16',
    }));
    console.log(this.items);
    this.com = new common(this.router);
    this.com.CheckLoginPortal();
    var a = this.com.getPortalInfo();
    this.IDCustomer = a.Info.UserID;
    this.token = a.Token;
    this.route.queryParams.subscribe((params) => {
      this.EventsID = params.EventsID;
    });
    var req = {
      IDCustommer:  this.IDCustomer,
      IDEvent: this.EventsID,
    };
    this.getAll(this.token, req);
    this.GetWinnerByEvent(this.token, req)
  }
  getAll(token, req) {
    this.service.GetGuestByCustomer(token, req).subscribe((z) => {
      console.log(z.Data);
      this.TableData = z.Data;
      const colors = ['#FF0000', '#000000'];
      this.items = this.TableData.map((value) => ({
        fillStyle: colors[value.ID % 2],
        text: value.HoTen,
        id: value.ID,
        textFillStyle: 'white',
        textFontSize: '16',
      }));
      this.reset();
    });
  }
  GetWinnerByEvent(token, req) {
    this.service.GetWinnerByEvent(token, req).subscribe((z) => {
      this.WinnerData = z.Data;
      this.dtTrigger.next();
      const colors = ['#FF0000', '#000000'];
      this.items = this.TableData.map((value) => ({
        fillStyle: colors[value.ID % 2],
        text: value.HoTen,
        id: value.ID,
        textFillStyle: 'white',
        textFontSize: '16',
      }));
      this.reset();
    });
  }
  AddWinner(token, req) {
    this.service.AddWinner(token, req).subscribe((z) => {
      if(z.Status==1){
        this.toastr.success('Xin chúc mừng ' + this.TableData[this.GuestIndex].HoTen);
        var req = {
          IDCustommer:  this.IDCustomer,
          IDEvent: this.EventsID,
        };
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
        });
        this.GetWinnerByEvent(this.token, req)
      }else{
        this.toastr.warning(z.Message)
      }
      
    });
  }
  reset() {
    this.wheel.reset();
  }
  before() { }

  async spin(prize) {
    this.idToLandOn = prize;
    await new Promise((resolve) => setTimeout(resolve, 0));
    this.wheel.spin();
  }
  Start() {
    if(this.WinnerData.length<this.TableData.length){
      this.reset();
      this.GuestIndex = this.getRandomInt(this.TableData.length)
      this.spin(this.TableData[this.GuestIndex].ID)
    }
    else{
      this.toastr.warning("Số lượng khách may mắn đã đến giới hạn!")
    }
    
  }
  
  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }
  after() {
    if(this.WinnerData.length<this.TableData.length){
      
    var req = {
      IDGuest:   this.TableData[this.GuestIndex].ID,
      IDEvent: this.EventsID,
    };
    this.AddWinner(this.token, req);
    }
    else{
      this.toastr.warning("Số lượng khách may mắn đã đến giới hạn!")
    }
    
  }
}
