import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { common } from 'src/app/app.common';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { PortalService } from 'src/app/utils/services/portal.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-checkin-scan',
  templateUrl: './checkin-scan.component.html',
  styleUrls: ['./checkin-scan.component.scss'],
})
export class CheckinScanComponent implements OnInit {
  EventsID: any;
  token: any;
  NoiDung: any;
  TenSuKien: any;
  DiaChi: any;
  GhiChu: any;
  Insert = new FormGroup({
    TenSuKien: new FormControl(null),
    NoiDung: new FormControl(null),
    DiaDiem: new FormControl(null),
    GhiChu: new FormControl(null),
  });
  public qrdata: string = null;
  public scale: number = 1;
  public elementType: 'img';
  constructor(
    private toastr: ToastrService,
    public router: Router,
    public route: ActivatedRoute,
    private service: PortalService
  ) {
    this.qrdata = 'http://localhost:4200/checkin?IDSuKien=0';
  }
  public com: common;

  ngOnInit(): void {
    this.com = new common(this.router);
    this.com.CheckLoginPortal();
    var a = this.com.getPortalInfo();
    this.token = a.Token;
    this.route.queryParams.subscribe((params) => {
      this.qrdata = 'http://localhost:4200/checkin?IDSuKien=' + params.EventsID;
      this.EventsID = params.EventsID;
      this.GetEvent(this.EventsID, this.token);
    });
  }
  GetEvent(ID, token) {
    this.service.GetEvent(ID, token).subscribe((z) => {
      this.Insert.patchValue({
        TenSuKien: z.Data.TenSuKien,
        NoiDung: z.Data.NoiDung,
        DiaDiem: z.Data.DiaDiem,
        GhiChu: z.Data.GhiChu,
      });
    });
  }
}
