import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckinScanComponent } from './checkin-scan.component';

describe('CheckinScanComponent', () => {
  let component: CheckinScanComponent;
  let fixture: ComponentFixture<CheckinScanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckinScanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckinScanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
