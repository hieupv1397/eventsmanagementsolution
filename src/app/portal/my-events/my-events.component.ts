import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { common } from 'src/app/app.common';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { PortalService } from 'src/app/utils/services/portal.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
import Alignment from '@ckeditor/ckeditor5-alignment/src/alignment';
@Component({
  selector: 'app-my-events',
  templateUrl: './my-events.component.html',
  styleUrls: ['./my-events.component.scss']
})
export class MyEventsComponent implements OnInit {

  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  dtOptions:any={};
  token: string;
  UserName: string;
  TableData: any;
  TitleClass: string = 'spinner-border text-muted';
  closeResult: string;
  isEdit: any;
  configCkediter: any;
  IDCustomer: any;
  public editorData = '<p>Nhập nội dung sự kiện tại đây...</p>'
  submitted = false;
  constructor(
    private toastr: ToastrService,
    public router: Router,
    private service: PortalService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService
  ) {
    this.configCkediter = {uiColor: '#99000'};
   }
  Insert = new FormGroup({
    ID: new FormControl(0),
    TenSuKien: new FormControl(null, [Validators.required]),
    NoiDung: new FormControl(null, [Validators.required]),
    DiaDiem: new FormControl(null, [Validators.required]),
    ThoiGian: new FormControl(null),
    TrangThai: new FormControl(false),
    GhiChu: new FormControl(null),
    IDCustomer: new FormControl(null),
  });
  public com: common;
  dtTrigger = new Subject();
  ngOnInit(): void {
   
    this.com = new common(this.router);
    this.com.CheckLoginPortal();
    var a = this.com.getPortalInfo();
    this.UserName = a.Info.UserName;
    this.IDCustomer = a.Info.UserID;
    this.token = a.Token;
    var RequestLogin = {
      RequestID: a.Info.UserID
    }
    this.getAll(this.token, RequestLogin);
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: true,
      dom: 'Bfrtip',
      // Configure the buttons
      buttons: [
        'excel',
      ],
      language: {
        processing: 'Đang xử lý...',
        lengthMenu: 'Xem _MENU_ mục',
        emptyTable: 'Không có dữ liệu!',
        info: 'Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục',
        infoEmpty: 'Đang xem 0 đến 0 trong tổng số 0 mục',
        infoFiltered: '(được lọc từ _MAX_ mục)',
        infoPostFix: '',
        search: 'Tìm kiếm nhanh:',
        url: '',
        searchPlaceholder: 'Nhập từ khóa cần tìm...',
        paginate: {
          first: 'Đầu',
          previous: 'Trước',
          next: 'Tiếp',
          last: 'Cuối',
        },
      },
      columns: [
        {
          title: 'Mã sự kiện',
        },
        {
          title: 'Tên khách hàng',
          visible: false,
        },
        {
          title: 'Tên sự kiện',
        },
        {
          title: 'Nội dung',
          visible: false,
        },
        {
          title: 'Địa chỉ',
        },
        {
          title: 'Thời gian',
        },
        {
          title: 'Trạng thái',
        },
        {
          title: 'Ghi chú',
        },
        {
          title: 'Tác vụ',
          width: '15%',
          className: 'dt-center',
        },
      ],
    };
  }
  
  public Editor = ClassicEditor;
  open(content, isEdit, data) {

    this.resetForm()
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl', scrollable: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    this.isEdit = false;
    if (isEdit) {
      this.isEdit = true;
      console.log(data)
     this.editorData=data.NoiDung;
      this.Insert.patchValue({
        ID: data.ID,
        TenSuKien: data.TenSuKien,
        NoiDung: data.NoiDung,
        DiaDiem: data.DiaDiem,
        ThoiGian: data.ThoiGian,
        TrangThai: data.TrangThai,
        GhiChu: data.GhiChu,
      });
    }

  }
  onChange({ editor }: ChangeEvent) {
    this.Insert.patchValue({
      NoiDung: editor.getData()
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  resetForm() {
    this.Insert.patchValue({
      ID: 0,
      TenSuKien: "",
      NoiDung: "",
      DiaDiem: "",
      TrangThai: "",
      GhiChu: "",
      IDCustomer:this.IDCustomer
    });
  }
  getAll(token, req) {
    this.service.GetAllByCustomer(token, req).subscribe((z) => {
      this.TableData = z.Data;
      this.dtTrigger.next();
      this.TitleClass = '';
    });
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    $.fn['dataTable'].ext.search.pop();
  }
  InsertData(){
    this.spinner.show();
    this.service.Insert(this.Insert.value, this.token).subscribe((z) => {
      this.spinner.hide();
      if (z.Status == 1) {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
        });
        var RequestLogin = {
          RequestID: this.IDCustomer
        }
        this.getAll(this.token, RequestLogin);
        this.resetForm();
        this.modalService.dismissAll('mymodal');
        this.toastr.success(z.Message);
      } else{
        this.toastr.error(z.Message);
      } 
    });
  }
  UpdateData(){
    this.spinner.show();
    this.service.Update(this.Insert.value, this.token).subscribe((z) => {
      this.spinner.hide();
      if (z.Status == 1) {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
        });
        var RequestLogin = {
          RequestID: this.IDCustomer
        }
        this.getAll(this.token, RequestLogin);
        this.resetForm();
        this.modalService.dismissAll('mymodal');
        this.toastr.success(z.Message);
      } else{
        this.toastr.error(z.Message);
      } 
    });
  }
  Delete(){
    this.spinner.show();
    this.service.Delete(this.Insert.value, this.token).subscribe((z) => {
      this.spinner.hide();
      if (z.Status == 1) {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
        });
        var RequestLogin = {
          RequestID: this.IDCustomer
        }
        this.getAll(this.token, RequestLogin);
        this.resetForm();
        this.toastr.success(z.Message);
      } else{
        this.toastr.error(z.Message);
      } 
    });
  }
  DeleteConfirm(data){
    var r=confirm("Bạn có muốn xóa sự kiện này không?")
    if(r){
      this.Insert.patchValue({
        ID:data.ID,
        HoTen: data.HoTen,
        UserName: data.UserName,
        Email: data.Email,
        isPremium: data.isPremium,
      });
      this.Delete()
    }
  }
  onSubmit() {
    this.submitted = true;
    console.log(this.Insert)
    if (this.Insert.valid) {
      if(this.isEdit){
        this.UpdateData()
      }else{
        this.InsertData();
      }
      
    }

  }
  get checkvalue() {
    return this.Insert.controls;
  }
}
