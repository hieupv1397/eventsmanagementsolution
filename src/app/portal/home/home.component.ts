import { Component, OnInit, ViewChild } from '@angular/core';
import { common } from 'src/app/app.common';
import { AppService } from 'src/app/utils/services/app.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { PortalService } from 'src/app/utils/services/portal.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public PortalInfor: any;
  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  dtOptions: DataTables.Settings
  TableData: any;
  DotDanhGia: string;
  TrangThaiFT: string = "0";
  GiangVien: string;
  PhieuDanhGia: string;
  closeResult: string;
  Token: string;
  DetailData: any;
  Detail: any;
  ListKetQuas = []
  SelectedID: any;
  Nhom_danh_gia: string;
  public CauHois: any;
  public PhuongAns: any;
  public KetQuas: any;
  TenPhieu: any;
  public DataAnwser: any;
  Title: string = "Đang tải danh sách thông báo..."
  TitleClass: string = "spinner-border text-muted"
  constructor(public appService: AppService,
    private toastr: ToastrService,
    public router: Router,
    private Ser: PortalService,
    private modalService: NgbModal,
  ) { }
  public com: common;
  dtTrigger = new Subject();
  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 25,
      ordering: false,
      language: {
        processing: "Đang xử lý...",
        lengthMenu: "Xem _MENU_ mục",
        emptyTable: "Không có dữ liệu!",
        info: "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
        infoEmpty: "Đang xem 0 đến 0 trong tổng số 0 mục",
        infoFiltered: "(được lọc từ _MAX_ mục)",
        infoPostFix: "",
        search: "Tìm kiếm nhanh:",
        url: "",
        searchPlaceholder: "Nhập từ khóa cần tìm...",
        paginate: {
          first: "Đầu",
          previous: "Trước",
          next: "Tiếp",
          last: "Cuối"
        }

      },
      columns: [
        {
          title: 'Đợt đánh giá',
        },
        {
          title: 'Phiếu đánh giá',
        },
        {
          title: 'Giảng viên',
        },
        {
          title: 'Môn học',
        },
        {
          title: 'Ngày bắt đầu',
        },
        {
          title: 'Ngày kết thúc',
        },
        {
          title: 'Trạng thái',
        },
        {
          title: 'Người gửi',
        },
        {
          title: 'Thao tác',
          width: "5%",
          className: "dt-center"
        },
      ],
    };
    $.fn['dataTable'].ext.search.push((settings, data, dataIndex) => {
      console.log(data)
      let DotDT = data[0];
      let PhieuDanhGiaDT = data[1];
      let TrangThaiDT = data[6];
      let GiangVienDT = data[2];
      let bool1 = true
      let bool2 = true
      let bool3 = true
      let bool4 = true
      let fillter = true;
      if (this.DotDanhGia != undefined) {
        bool1 = DotDT.includes(this.DotDanhGia)
      }
      if (this.PhieuDanhGia != undefined) {
        bool2 = PhieuDanhGiaDT.includes(this.PhieuDanhGia)
      }
      if (this.TrangThaiFT != "0") {
        bool3 = TrangThaiDT.includes(this.TrangThaiFT)
      }
      if (this.GiangVien != undefined) {
        bool4 = GiangVienDT.includes(this.GiangVien)
      }
      fillter = bool1 && bool2 && bool3 && bool4
      return fillter;
    });
    this.com = new common(this.router);
    this.PortalInfor = this.com.getPortalInfo();
    this.Token = this.PortalInfor.Token;
    var RequestLogin = {
      RequestID: this.PortalInfor.Info.UserID
    }

    this.GetAllByCustomer(this.Token,RequestLogin);
  }
  filterById(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }
  GetAllByCustomer(m,data) {
    this.Ser.GetAllByCustomer(m,data)
      .subscribe(z => {
        this.TableData = z.Data
        this.dtTrigger.next();
        this.Title = ""
        this.TitleClass = ""
      })
      ;
  }
  openEdit(content, IDGui, IDPhieuDanhGia, HetHan) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl', scrollable: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    }); 
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
