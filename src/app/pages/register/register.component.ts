import { Component, OnInit, Renderer2, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppService } from 'src/app/utils/services/app.service';
import { ToastrService } from 'ngx-toastr';
import { PortalService } from 'src/app/utils/services/portal.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  public registerForm: FormGroup;
  constructor(
    private renderer: Renderer2,
    private toastr: ToastrService,
    private service: PortalService,
    private appService: AppService
  ) {}

  ngOnInit() {
    this.renderer.addClass(document.querySelector('app-root'), 'register-page');
    this.registerForm = new FormGroup({
      IDSuKien: new FormControl(null),
      HoTen: new FormControl(null, Validators.required),
      SoDienThoai: new FormControl(null, Validators.required),
      Email: new FormControl(null, Validators.required),
      DiaChi: new FormControl(null, Validators.required),
    });
  }

  register() {
    if (this.registerForm.valid) {
      console.log(this.registerForm.value);
    } else {
      this.toastr.error(
        'Thông tin không chính xác',
        'Vui lòng kiểm tra lại thông tin của bạn'
      );
    }
  }
  CheckIn(req) {
    this.service.CheckIn(req).subscribe((z) => {
      console.log(z.Data);
    });
  }
}
