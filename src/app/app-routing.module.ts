import { NgModule } from '@angular/core';
import { Routes, RouterModule, GuardsCheckStart } from '@angular/router';
import { MainComponent } from './pages/main/main.component';
import { BlankComponent } from './views/blank/blank.component';
import { LoginComponent } from './pages/login/login.component';
import { ProfileComponent } from './views/profile/profile.component';
import { RegisterComponent } from './pages/register/register.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { AuthGuard } from './utils/guards/auth.guard';
import { NonAuthGuard } from './utils/guards/non-auth.guard';
import { EventsComponent } from './views/events/events.component';
import { CustomerComponent } from './views/customer/customer.component';
import { PortalComponent } from './portal/portal.component';
import { HomeComponent } from './portal/home/home.component';
import { LoginPortalComponent } from './portal/login-portal/login-portal.component';
import { MyEventsComponent } from './portal/my-events/my-events.component';
import { ReportComponent } from './portal/report/report.component';
import { CheckinScanComponent } from './portal/checkin-scan/checkin-scan.component';
import { CheckinComponent } from './portal/checkin/checkin.component';
import { GuestComponent } from './portal/guest/guest.component';
import { ReportAdminComponent } from './views/report-admin/report-admin.component';
import { GameComponent } from './portal/game/game.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { RegisterPortalComponent } from './portal/register-portal/register-portal.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {
        path: 'events',
        component: EventsComponent,
      },
      {
        path: 'customer',
        component: CustomerComponent,
      },
      {
        path: 'profile',
        component: ProfileComponent,
      },
      {
        path: 'report-admin',
        component: ReportAdminComponent,
      },
      {
        path: 'blank',
        component: BlankComponent,
      },
      {
        path: '',
        component: DashboardComponent,
      },
    ],
  },
  {
    path: 'portal',
    component: PortalComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {
        path: 'customer',
        component: HomeComponent,
      },
      {
        path: 'my-events',
        component: MyEventsComponent,
      },
      {
        path: 'game',
        component: GameComponent,
      },
      {
        path: 'report',
        component: ReportComponent,
      },
      {
        path: 'checkin-scan',
        component: CheckinScanComponent,
      },
      {
        path: 'guest-detail',
        component: GuestComponent,
      },
    ],
  },
  {
    path: 'login-portal',
    component: LoginPortalComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'register-portal',
    component: RegisterPortalComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'checkin',
    component: CheckinComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [NonAuthGuard],
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [NonAuthGuard],
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    BrowserModule,
    HttpClientModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}
