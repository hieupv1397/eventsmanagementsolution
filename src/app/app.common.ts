import { ResponseLogin } from 'src/app/Models/output.model/ResponseLogin';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

export class common {
  public LoginResult: ResponseLogin;
  public PortalResult: any;
  public cookieService: CookieService;
  public CheckLogin() {
    this.LoginResult = new ResponseLogin();
    this.LoginResult = this.getUserinfo();
    if (this.LoginResult == null) {
      this.router.navigate(['/login']);
    }
  }
  constructor(private router: Router) { }
  public getUserinfo() {
    this.LoginResult = JSON.parse(localStorage.getItem('UserInfo'));
    return this.LoginResult;
  }
  public CheckLoginPortal() {
    this.PortalResult = null;
    this.PortalResult = this.getPortalInfo();
    if (this.PortalResult == null) {
      this.router.navigate(['/login-portal']);
    }
  }
  public getPortalInfo() {
    this.PortalResult = JSON.parse(localStorage.getItem('PortalInfo'))
    return this.PortalResult;
}
  login() {
    this.router.navigate(['/']);
  }
  logout() {
    localStorage.removeItem('UserInfo');
    this.router.navigate(['/login']);
  }
}
