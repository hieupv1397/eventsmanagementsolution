import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { common } from 'src/app/app.common';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { PortalService } from 'src/app/utils/services/portal.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger = new Subject();
  token: string;
  TableData: any;
  EventsID: any;
  Customers: any;
  Events: any;
  Guests: any;
  public com: common;
  constructor(
    private toastr: ToastrService,
    public router: Router,
    private service: PortalService,
    private modalService: NgbModal,
    public route: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) { }
  ngOnInit(): void {
    this.com = new common(this.router);
    this.com.CheckLogin();
    var a = this.com.getUserinfo();
    this.token = a.Token;
    this.getDashBoard(this.token);

  }
  getDashBoard(token) {
    this.service.DashBoard(token).subscribe((z) => {
      console.log(z);
      this.Customers=z.Customers;
      this.Guests=z.Guests;
      this.Events=z.Events;
    });
  }
}
