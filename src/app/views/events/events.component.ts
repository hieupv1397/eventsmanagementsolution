import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { common } from 'src/app/app.common';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { EventsService } from 'src/app/utils/services/events.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss'],
})
export class EventsComponent implements OnInit, OnDestroy {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  dtOptions:any={};
  token: string;
  UserName: string;
  TableData: any;
  TitleClass: string = 'spinner-border text-muted';
  constructor(
    private toastr: ToastrService,
    public router: Router,
    private service: EventsService,
    private modalService: NgbModal
  ) {}
  public com: common;
  dtTrigger = new Subject();
  ngOnInit(): void {
    this.com = new common(this.router);
    this.com.CheckLogin();
    var a = this.com.getUserinfo();
    this.UserName = a.Info.UserName;
    this.token = a.Token;

    this.getAll(this.token);
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: true,
      language: {
        processing: 'Đang xử lý...',
        lengthMenu: 'Xem _MENU_ mục',
        emptyTable: 'Không có dữ liệu!',
        info: 'Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục',
        infoEmpty: 'Đang xem 0 đến 0 trong tổng số 0 mục',
        infoFiltered: '(được lọc từ _MAX_ mục)',
        infoPostFix: '',
        search: 'Tìm kiếm nhanh:',
        url: '',
        searchPlaceholder: 'Nhập từ khóa cần tìm...',
        paginate: {
          first: 'Đầu',
          previous: 'Trước',
          next: 'Tiếp',
          last: 'Cuối',
        },
      },
      dom: 'Bfrtip',
      // Configure the buttons
      buttons: [
        'excel',
      ],
      columns: [
        {
          title: 'Mã sự kiện',
        },
        {
          title: 'Tên khách hàng',
        },
        {
          title: 'Tên sự kiện',
        },
        {
          title: 'Địa chỉ',
        },
        {
          title: 'Thời gian',
        },
        {
          title: 'Trạng thái',
        },
        {
          title: 'Ghi chú',
        },
      ],
    };
  }
  getAll(token) {
    this.service.GetAll(token).subscribe((z) => {
      this.TableData = z.Data;
      this.dtTrigger.next();
      this.TitleClass = '';
    });
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    $.fn['dataTable'].ext.search.pop();
  }
}
