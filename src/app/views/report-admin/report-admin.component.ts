import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { common } from 'src/app/app.common';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { PortalService } from 'src/app/utils/services/portal.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-report-admin',
  templateUrl: './report-admin.component.html',
  styleUrls: ['./report-admin.component.scss'],
})
export class ReportAdminComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger = new Subject();
  token: string;
  TableData: any;
  EventsID: any;
  public com: common;
  constructor(
    private toastr: ToastrService,
    public router: Router,
    private service: PortalService,
    private modalService: NgbModal,
    public route: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) {}
  ngOnInit(): void {
    this.com = new common(this.router);
    this.com.CheckLogin();
    var a = this.com.getUserinfo();
    this.token = a.Token;
    this.getAll(this.token);
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: true,
      dom: 'Bfrtip',
      // Configure the buttons
      buttons: ['excel'],
      language: {
        processing: 'Đang xử lý...',
        lengthMenu: 'Xem _MENU_ mục',
        emptyTable: 'Không có dữ liệu!',
        info: 'Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục',
        infoEmpty: 'Đang xem 0 đến 0 trong tổng số 0 mục',
        infoFiltered: '(được lọc từ _MAX_ mục)',
        infoPostFix: '',
        search: 'Tìm kiếm nhanh:',
        url: '',
        searchPlaceholder: 'Nhập từ khóa cần tìm...',
        paginate: {
          first: 'Đầu',
          previous: 'Trước',
          next: 'Tiếp',
          last: 'Cuối',
        },
      },
      columns: [
        {
          title: 'ID',
        },
        {
          title: 'Họ tên',
        },
        {
          title: 'Tài khoản',
        },
        {
          title: 'Loại tài khoản',
        },
        {
          title: 'Email',
        },
        {
          title: 'Tổng sự kiện',
        },
        {
          title: 'Tổng khách tham dự',
        },
      ],
    };
  }
  getAll(token) {
    this.service.ThongKeKhachHang(token).subscribe((z) => {
      console.log(z.Data);
      this.TableData = z.Data;
      this.dtTrigger.next();
    });
  }
}
