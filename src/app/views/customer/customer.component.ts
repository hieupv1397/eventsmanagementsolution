import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { common } from 'src/app/app.common';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { CustomerService } from 'src/app/utils/services/customer.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
})
export class CustomerComponent implements OnInit, OnDestroy {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  dtOptions:any={};
  
  
  dataTable: any;
  token: string;
  UserName: string;
  TableData: any;
  isEdit:any;
  submitted = false;
  closeResult: string;
  TitleClass: string = 'spinner-border text-muted';
  Insert = new FormGroup({
    ID: new FormControl(0),
    HoTen: new FormControl(null, [Validators.required, Validators.maxLength(200),]),
    Email: new FormControl(null, [Validators.required, Validators.email,]),
    UserName: new FormControl(null, [Validators.required, Validators.maxLength(50),]),
    isPremium: new FormControl(false),
  });
  constructor(
    private toastr: ToastrService,
    public router: Router,
    private service: CustomerService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService
  ) { }
  public com: common;
  dtTrigger = new Subject();
  get checkvalue() {
    return this.Insert.controls;
  }
  ngOnInit(): void {
    this.com = new common(this.router);
    this.com.CheckLogin();
    var a = this.com.getUserinfo();
    this.UserName = a.Info.UserName;
    this.token = a.Token;

    this.getAll(this.token);
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: true,
      dom: 'Bfrtip',
      // Configure the buttons
      buttons: [
        'excel',
      ],
      language: {
        processing: 'Đang xử lý...',
        lengthMenu: 'Xem _MENU_ mục',
        emptyTable: 'Không có dữ liệu!',
        info: 'Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục',
        infoEmpty: 'Đang xem 0 đến 0 trong tổng số 0 mục',
        infoFiltered: '(được lọc từ _MAX_ mục)',
        infoPostFix: '',
        search: 'Tìm kiếm nhanh:',
        url: '',
        searchPlaceholder: 'Nhập từ khóa cần tìm...',
        paginate: {
          first: 'Đầu',
          previous: 'Trước',
          next: 'Tiếp',
          last: 'Cuối',
        },
      },
      columns: [
        {
          title: 'Mã khách hàng',
        },
        {
          title: 'Họ tên',
        },
        {
          title: 'Email',
        },
        {
          title: 'Tài khoản',
        },
        {
          title: 'Mật khẩu',
          visible: false,
        },
        {
          title: 'isPremium',
          visible: false,
        },
        {
          title: 'Loại tài khoản',
        },
        {
          title: 'Tác vụ',
          width: '14%',
          className: 'dt-center',
        },
      ],
    };
  }

  open(content, isEdit,data) {
    this.resetForm()
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl', scrollable: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    this.isEdit=false;
    if (isEdit) {
      this.isEdit=true;
      console.log(data)
      this.Insert.patchValue({
        ID:data.ID,
        HoTen: data.HoTen,
        UserName: data.UserName,
        Email: data.Email,
        isPremium: data.isPremium,
      });
    }

  }
  getAll(token) {
    this.service.GetAll(token).subscribe((z) => {
      this.TableData = z.Data;
      this.dtTrigger.next();
      this.TitleClass = '';
    });
  }
  resetForm(){
    this.Insert.patchValue({
      ID:0,
      HoTen: "",
      UserName: "",
      Email: "",
      isPremium: "",
    });
  }
  onSubmit() {
    this.submitted = true;
    console.log(this.Insert)
    if (this.Insert.valid) {
      if(this.isEdit){
        this.UpdateData()
      }else{
        this.InsertData();
      }
      
    }

  }
  InsertData(){
    this.spinner.show();
    this.service.Insert(this.Insert.value, this.token).subscribe((z) => {
      this.spinner.hide();
      if (z.Status == 1) {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
        });
        this.getAll(this.token);
        this.resetForm();
        this.modalService.dismissAll('mymodal');
        this.toastr.success(z.Message);
      } else{
        this.toastr.error(z.Message);
      } 
    });
  }
  UpdateData(){
    this.spinner.show();
    this.service.Update(this.Insert.value, this.token).subscribe((z) => {
      this.spinner.hide();
      if (z.Status == 1) {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
        });
        this.getAll(this.token);
        this.resetForm();
        this.modalService.dismissAll('mymodal');
        this.toastr.success(z.Message);
      } else{
        this.toastr.error(z.Message);
      } 
    });
  }
  Delete(){
    this.spinner.show();
    this.service.Delete(this.Insert.value, this.token).subscribe((z) => {
      this.spinner.hide();
      if (z.Status == 1) {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
        });
        this.getAll(this.token);
        this.resetForm();
        this.toastr.success(z.Message);
      } else{
        this.toastr.error(z.Message);
      } 
    });
  }
  DeleteConfirm(data){
    var r=confirm("Bạn có muốn xóa vĩnh viễn tài khoản này không?")
    if(r){
      this.Insert.patchValue({
        ID:data.ID,
        HoTen: data.HoTen,
        UserName: data.UserName,
        Email: data.Email,
        isPremium: data.isPremium,
      });
      this.Delete()
    }
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    $.fn['dataTable'].ext.search.pop();
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  openEdit(content) {

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl', scrollable: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });


  }
  openViewModal(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl', scrollable: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
}
