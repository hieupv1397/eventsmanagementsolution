import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class PortalService {
  constructor(private http: HttpClient) {}
  GetAllByCustomer(token, data): Observable<any> {
    return this.http
      .post<any>(environment.APIURL + 'SuKien/GetAllByCustomer', data, {
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
      })
      .pipe(
        map((z) => {
          return z;
        })
      );
  }
  GetGuestByCustomer(token, data): Observable<any> {
    return this.http
      .post<any>(environment.APIURL + 'SuKien/GetGuestByCustomer', data, {
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
      })
      .pipe(
        map((z) => {
          return z;
        })
      );
  }
  GetWinnerByEvent(token, data): Observable<any> {
    return this.http
      .post<any>(environment.APIURL + 'SuKien/GetWinnerByEvent', data, {
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
      })
      .pipe(
        map((z) => {
          return z;
        })
      );
  }
  AddWinner(token, data): Observable<any> {
    return this.http
      .post<any>(environment.APIURL + 'SuKien/AddWinner', data, {
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
      })
      .pipe(
        map((z) => {
          return z;
        })
      );
  }
  ThongKeSuKien(token, data): Observable<any> {
    return this.http
      .post<any>(environment.APIURL + 'SuKien/ThongKeSuKien', data, {
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
      })
      .pipe(
        map((z) => {
          return z;
        })
      );
  }
  ThongKeKhachHang(token): Observable<any> {
    return this.http
      .get<any>(environment.APIURL + 'SuKien/ThongKeKhachHang', {
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
      })
      .pipe(
        map((z) => {
          return z;
        })
      );
  }
  DashBoard(token): Observable<any> {
    return this.http
      .get<any>(environment.APIURL + 'SuKien/DashBoard', {
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
      })
      .pipe(
        map((z) => {
          return z;
        })
      );
  }
  Insert(req: any, token): Observable<any> {
    return this.http
      .post<any>(environment.APIURL + 'SuKien/Add', req, {
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
      })
      .pipe(
        map((z) => {
          return z;
        })
      );
  }
  Update(req: any, token): Observable<any> {
    return this.http
      .post<any>(environment.APIURL + 'SuKien/Edit', req, {
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
      })
      .pipe(
        map((z) => {
          return z;
        })
      );
  }
  Delete(req: any, token): Observable<any> {
    return this.http
      .post<any>(environment.APIURL + 'SuKien/Delete', req, {
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
      })
      .pipe(
        map((z) => {
          return z;
        })
      );
  }
  GetEvent(ID: any, token): Observable<any> {
    return this.http
      .get<any>(environment.APIURL + 'SuKien/GetEvent?ID=' + ID, {
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
      })
      .pipe(
        map((z) => {
          return z;
        })
      );
  }
  CheckIn(data): Observable<any> {
    return this.http
      .post<any>(environment.APIURL + 'SuKien/CheckIn', data, {})
      .pipe(
        map((z) => {
          return z;
        })
      );
  }
}
