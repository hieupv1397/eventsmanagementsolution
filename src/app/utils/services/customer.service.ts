import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  constructor(private http: HttpClient) {}
  GetAll(token): Observable<any> {
    return this.http
      .get<any>(environment.APIURL + 'KhachHang/GetAll', {
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
      })
      .pipe(
        map((z) => {
          return z;
        })
      );
  }
  Insert(req:any, token) : Observable<any>{
    return this.http.post<any>(environment.APIURL + 'KhachHang/Add',req,{
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${token}`)
      } ).pipe(map(z=> {return z}))
    }
    Update(req:any, token) : Observable<any>{
      return this.http.post<any>(environment.APIURL + 'KhachHang/Edit',req,{
        headers: new HttpHeaders()
          .set('Authorization', `Bearer ${token}`)
        } ).pipe(map(z=> {return z}))
      }
      Delete(req:any, token) : Observable<any>{
        return this.http.post<any>(environment.APIURL + 'KhachHang/Delete',req,{
          headers: new HttpHeaders()
            .set('Authorization', `Bearer ${token}`)
          } ).pipe(map(z=> {return z}))
        }
}
