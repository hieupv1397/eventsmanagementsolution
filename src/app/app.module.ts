import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './pages/main/main.component';
import { LoginComponent } from './pages/login/login.component';
import { HeaderComponent } from './pages/main/header/header.component';
import { FooterComponent } from './pages/main/footer/footer.component';
import { MenuSidebarComponent } from './pages/main/menu-sidebar/menu-sidebar.component';
import { BlankComponent } from './views/blank/blank.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProfileComponent } from './views/profile/profile.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './pages/register/register.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { ToastrModule } from 'ngx-toastr';
import { MessagesDropdownMenuComponent } from './pages/main/header/messages-dropdown-menu/messages-dropdown-menu.component';
import { NotificationsDropdownMenuComponent } from './pages/main/header/notifications-dropdown-menu/notifications-dropdown-menu.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppButtonComponent } from './components/app-button/app-button.component';
import { registerLocaleData, LocationStrategy, HashLocationStrategy } from '@angular/common';
import localeEn from '@angular/common/locales/en';
import { UserDropdownMenuComponent } from './pages/main/header/user-dropdown-menu/user-dropdown-menu.component';
import { HttpClientModule } from '@angular/common/http';
import { CustomerComponent } from './views/customer/customer.component';
import { EventsComponent } from './views/events/events.component';
import { DataTablesModule } from 'angular-datatables';
import { PortalComponent } from './portal/portal.component';
import { LoginPortalComponent } from './portal/login-portal/login-portal.component';
import { HomeComponent } from './portal/home/home.component';
import { HeaderPortalComponent } from './portal/header-portal/header-portal.component';
import { MenuSidebarPortalComponent } from './portal/menu-sidebar-portal/menu-sidebar-portal.component';
import { FooterPortalComponent } from './portal/footer-portal/footer-portal.component';
import { MyEventsComponent } from './portal/my-events/my-events.component';
import { ReportComponent } from './portal/report/report.component';
import { CheckinScanComponent } from './portal/checkin-scan/checkin-scan.component';
import { QRCodeModule } from 'angularx-qrcode';
import { NgxSpinnerModule } from 'ngx-spinner';
import { GuestComponent } from './portal/guest/guest.component';
import { CheckinComponent } from './portal/checkin/checkin.component';
import { ReportAdminComponent } from './views/report-admin/report-admin.component';
import { NgxWheelModule } from 'ngx-wheel';
import { GameComponent } from './portal/game/game.component';
import { CookieService } from 'ngx-cookie-service';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { RegisterPortalComponent } from './portal/register-portal/register-portal.component';
registerLocaleData(localeEn, 'en-EN');

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    MenuSidebarComponent,
    BlankComponent,
    ProfileComponent,
    RegisterComponent,
    DashboardComponent,
    MessagesDropdownMenuComponent,
    NotificationsDropdownMenuComponent,
    AppButtonComponent,
    UserDropdownMenuComponent,
    CustomerComponent,
    EventsComponent,
    PortalComponent,
    LoginPortalComponent,
    HomeComponent,
    FooterComponent,
    HeaderPortalComponent,
    FooterPortalComponent,
    MenuSidebarPortalComponent,
    MyEventsComponent,
    ReportComponent,
    CheckinScanComponent,
    GuestComponent,
    CheckinComponent,
    ReportAdminComponent,
    GameComponent,
    RegisterPortalComponent,
  ],
  imports: [
    CKEditorModule,
    HttpClientModule,
    NgxWheelModule,
    BrowserModule,
    AppRoutingModule,
    NgxSpinnerModule,
    DataTablesModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    QRCodeModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
    NgbModule,
  ],
  providers: [CookieService,
    {provide : LocationStrategy , useClass: HashLocationStrategy}],
  bootstrap: [AppComponent],
})
export class AppModule {}
